# Script zum Anzeigen der CO2 Konzentration von Thomas Wenzlaff
# Parameter JSon Object mit co2 in ppm und in der zweiten Zeile die Temperatur in Grad

import time
import Adafruit_GPIO.SPI as SPI
import Adafruit_SSD1306
import sys
import json
import subprocess
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

# Das JSon das gelesen wird: 
# { "messung":{"satz": [{"zeitpunkt":"17.10.2020 15:29" },{"SS": 0, "UhUl": 2048, "TT": 64, "co2": 402, "temperature": 24} ]}}
input_json = json.loads(sys.argv[1])
messung_json = input_json['messung']
satz_json = messung_json['satz']
satz_array_json = satz_json[1]
co2_wert = satz_array_json['co2']
temperatur_wert = satz_array_json['temperature']

RST = None 
DC = 23
SPI_PORT = 0
SPI_DEVICE = 0

disp = Adafruit_SSD1306.SSD1306_128_32(rst=RST)
disp.begin()
# disp.clear()
# disp.display()

width = disp.width
height = disp.height
image = Image.new('1', (width, height))

draw = ImageDraw.Draw(image)

padding = 0 
top = padding
bottom = height-padding
x = 0

font = ImageFont.truetype('/home/pi/twco2ampel/src/main/scripts/VCR_OSD_MONO_1.001.ttf', 18)

draw.rectangle((0,0,width,height), outline=0, fill=0)
draw.text((x, top),       "CO2 "  + str(co2_wert) + " ppm" ,  font=font, fill=255)
draw.text((x, top+16),    "Temp " + str(temperatur_wert) + " C", font=font, fill=255)

disp.image(image)
disp.display()
