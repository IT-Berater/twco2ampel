# 
# Script zum senden der CO2 und Temperatur Werte an Ubidots.com
# 
# Thomas Wenzlaff http://www.wenzlaff.info

import time
import requests
import math
import random
import sys
import json

API_TOKEN = "BBFF-..."
DEVICE_LABEL = "pi-bplus"
VARIABLE_LABEL_1 = "temperatur"
VARIABLE_LABEL_2 = "co2"
VARIABLE_LABEL_3 = "position"
# Position von Langenhagen, latitude und longitude
LAT = 52.44758
LNG = 9.73741

def build_payload(variable_1, variable_2, variable_3):
    # Das JSon das gelesen wird:
    # {"SS": 0, "UhUl": 2048, "TT": 64, "co2": 402, "temperature": 24}
    input_json = json.loads(sys.argv[1])
    co2  = input_json['co2']
    temp  = input_json['temperature']
        
    payload = {variable_1: temp,
               variable_2: co2,
               variable_3: {"value": 1, "context": {"lat": LAT, "lng": LNG}}}

    return payload

def post_request(payload):
    url = "http://industrial.api.ubidots.com"
    url = "{}/api/v1.6/devices/{}".format(url, DEVICE_LABEL)
    headers = {"X-Auth-API_TOKEN": API_TOKEN, "Content-Type": "application/json"}

    status = 400
    attempts = 0
    while status >= 400 and attempts <= 5:
        req = requests.post(url=url, headers=headers, json=payload)
        status = req.status_code
        attempts += 1
        time.sleep(1)

    if status >= 400:
        print("[ERROR] Could not send data after 5 attempts, please check \
            your API_TOKEN credentials and internet connection")
        return False
    return True

def main():
    payload = build_payload(
        VARIABLE_LABEL_1, VARIABLE_LABEL_2, VARIABLE_LABEL_3)

    print("[INFO] Sende Daten an ubidots.com ...")
    post_request(payload)
    print("[INFO] gesendet")

if __name__ == '__main__':
        main()
