#!/bin/sh

# Sendet jede alle 1 Minuten die co2 Werte an MQTT NODERED
while (true) do
   
   zeit=$(date +"%d.%m.%Y %H:%M")
   wert=$(sudo python -m mh_z19 --all 2>%1)
   echo "Sende CO2 Messung: $wert um $zeit"
   # erstellen ein JSON Objekt
   sende="{ \"messung\":{\"satz\": [{\"zeitpunkt\":\"$zeit\" },$wert ]}}"

   mosquitto_pub -d -t co2 -m "$sende"
   
   # auf der Anzeige ausgeben
   echo "$sende" 
   python /home/pi/twco2ampel/src/main/scripts/co2ampel.py "$sende"
   echo "$wert"
   python /home/pi/twco2ampel/src/main/scripts/co2jsontorgb.py "$wert"   
   python /home/pi/twco2ampel/src/main/scripts/send-to-ubidots-com.py "$wert"
   sleep 60;
done;

