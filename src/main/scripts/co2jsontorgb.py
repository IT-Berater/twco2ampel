# Schaltet die RGB LED je nach inhalt des JSons mit CO2 Wert
# Als Parameter wird ein JSon Objekt uebergeben 

import time, sys
import RPi.GPIO as GPIO
import json

redPin = 11 
greenPin = 13
bluePin = 15

def blink(pin):
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(pin, GPIO.OUT)
    GPIO.output(pin, GPIO.HIGH)
    
def turnOff(pin):
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(pin, GPIO.OUT)
    GPIO.output(pin, GPIO.LOW)
    
def redOn():
    blink(redPin)

def redOff():
    turnOff(redPin)

def greenOn():
    blink(greenPin)

def greenOff():
    turnOff(greenPin)

def blueOn():
    blink(bluePin)

def blueOff():
    turnOff(bluePin)

def yellowOn():
    blink(redPin)
    blink(greenPin)

def yellowOff():
    turnOff(redPin)
    turnOff(greenPin)

def cyanOn():
    blink(greenPin)
    blink(bluePin)

def cyanOff():
    turnOff(greenPin)
    turnOff(bluePin)

def magentaOn():
    blink(redPin)
    blink(bluePin)

def magentaOff():
    turnOff(redPin)
    turnOff(bluePin)

def whiteOn():
    blink(redPin)
    blink(greenPin)
    blink(bluePin)

def whiteOff():
    turnOff(redPin)
    turnOff(greenPin)
    turnOff(bluePin)
    
def main():
 # Das JSon das gelesen wird:
 # {"SS": 0, "UhUl": 2048, "TT": 64, "co2": 402, "temperature": 24}
 input_json = json.loads(sys.argv[1])
 ppm = input_json['co2']

 GPIO.setwarnings(False)
 n = len(sys.argv)
 whiteOff()
 if ppm < 500:
      whiteOff()
 elif ppm >= 500 and ppm < 800:
      greenOn()
 elif ppm >= 800 and ppm < 1000:
      blueOn()
 elif ppm >= 1000:
      redOn()
        
main()
